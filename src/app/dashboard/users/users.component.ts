
import {UsersService } from '../../services/users.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { FieldConfig } from 'src/app/partials/formulaire/fields-interface';
import { FormulaireDynamiqueComponent } from './../../partials/formulaire/formulaire-dynamique/formulaire-dynamique.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  @ViewChild(FormulaireDynamiqueComponent) form: FormulaireDynamiqueComponent;
  utilConfig: FieldConfig[] = [
    {
      type: "input",
      label: "Nom d'utilisateur ",
      inputType: "text",
      name: "NomUser",
      validations: [
        {
          name: "required",
          validator: Validators.required,
          message: "Champ obligatoire"
        }
      ]
    },
    {
      type: "input",
      label: "email",
      inputType: "text",
      name: "Email",
      validations: [
        {
          name: "required",
          validator: Validators.required,
          message: "Champ obligatoire"
        },
  
      ]
    },
    {
      type: "input",
      label: "Telephone ",
      inputType: "text",
      name: "Tel",
      validations: [
        {
          name: "required",
          validator: Validators.required,
          message: "Champ obligatoire"
        }
      ]
    },

    {
      type: "input",
      label: "Mot de Passe",
      inputType: "password",
      name: "password",
      validations: [
        {
          name: "required",
          validator: Validators.required,
          message: "Champ obligatoire"
        },
        {
          name: "minlength",
          validator: Validators.minLength(6),
          message: "Au moins 6 caractères"
        }
      ]

      
    },



    {
      type: "button",
      label: "Annuler",
      inputType: "button",
      classe: "btn btn-primary",
      alignerContainer: true
    },
    {
      type: "button",
      label: "Valider",
      inputType: "submit",
      classe: "btn btn-success",
      alignerContainer: true
    },

  ];
  constructor(private UsersService: UsersService) { }

  ngOnInit(): void {
  }
    /**
   * fonction appelée lors du submit du formulaire
    }*/
  submit(valeurs: any) {
    var obj = valeurs.form.value;
    this.UsersService.saveUsers(obj. nom, obj.email, obj.Telephone,obj.Profil, obj.password).subscribe(
      res => {
        console.log("res : " +res);
      }
    
    )
    }
}
