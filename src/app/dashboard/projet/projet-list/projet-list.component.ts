import { ProjetService } from './../../../services/projet.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-projet-list',
  templateUrl: './projet-list.component.html',
  styleUrls: ['./projet-list.component.css']
})

export class ProjetListComponent implements OnInit {

  tab = [];
  constructor(private projetService: ProjetService) { }

  ngOnInit(): void {
    this.projetService.getAllProjet().subscribe( res => this.tab = res)
  }

  

}
