import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

const API_BASE_URL = "http://localhost/invest";

@Injectable({
  providedIn: 'root'
})

export class ProjetService {

  constructor(private http: HttpClient) { }

  saveProjet(nom: string, description: string, estimation: string, lieu: string): Observable<any> {
    let params = new HttpParams();
    params = params.append('nom', nom);
    params = params.append('description', description);
    params = params.append('estimation', estimation);
    params = params.append('lieu', lieu);
    return this.http.post(API_BASE_URL + "/projet/add.php", params);
  }

  getAllProjet(): Observable<any> {
    return this.http.get(API_BASE_URL + "/projet/all.php");
  }
}
