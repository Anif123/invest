import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

const API_BASE_URL = "http://localhost/invest";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  saveUsers(nom: string, email: string, Telephone: string,Profil: string, password: string): Observable<any> {
    let params = new HttpParams();
    params = params.append('nom', nom);
    params = params.append('email', email);
    params = params.append('Telephone', Telephone);
    params = params.append('Profil', Profil);
    params = params.append('password', password);
    return this.http.post(API_BASE_URL + "/users/add.php", params);
  }

}
